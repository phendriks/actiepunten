<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Overzicht - Actiepunten - KPN</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/base.css" type="text/css" />
<link rel="stylesheet" href="css/grid.css" type="text/css" />

<link rel="stylesheet" href="//cdn.datatables.net/1.10.1/css/jquery.dataTables.css" type="text/css" />
<link rel="stylesheet" href="css/themes/light.css" type="text/css" />
<link rel="stylesheet" href="css/themes/green.css" type="text/css" />
<link rel="stylesheet" href="js/themes/metro/jtable_metro_base.min.css" type="text/css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" href="js/themes/metro/green/jtable.min.css" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="js/jquery.jtable.min.js"></script>
<script src="js/localization/jquery.jtable.nl-NL.js"></script>
<script src="//cdn.datatables.net/1.10.1/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#overzicht').dataTable();
} );
</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
    <li><a href="index.php">Overzicht</a></li>
  <li class="current"><a href="actiepunten.php">Actiepunten</a><ul><li><a href="invoeren.php">Actiepunt invoeren</a></li><li><a href="archief.php">Archief</a></li></ul></li>
  <li><a href="medewerkers.php">Medewerkers</a></li>
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Archief</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
    <?php
	include("connector.php");
	
	$query = mysql_query("SELECT
								`a`.`id`,
								`a`.`titel`,
								`a`.`omschrijving`,
								`a`.`update_datum`,
								`a`.`eind_datum`,
								`a`.`eigenaar`,
								`a`.`categorie`,
								`a`.`status`,
								`a`.`datum_invoer`,
								`m`.`naam`
						FROM 
								`actiepunt` AS `a`
						LEFT JOIN `medewerkers` AS `m` ON `a`.`eigenaar` = `m`.`id`	
							where a.status = 'Afgesloten'");
			?>
			<table id="overzicht" class="display" cellspacing="0" width="100%">
			<thead><tr><th>Eigenaar</th><th>Titel</th><th>Update</th><th>Categorie</th><th>Invoer datum</th><th>Details</th></tr></thead>
			<?php
			while($db= mysql_fetch_assoc($query))
			{
				echo '<tr><td>'.$db['naam'].'</td><td>'.substr($db['titel'], 0, 50).'...</td><td>'.$db['update_datum'].'</td><td>'.$db['categorie'].'</td><td>'.$db['datum_invoer'].'</td><td><a href="actiepunten.php?id='.$db['id'].'">Details</a></td></tr>';
							
			};
			?>
			</table>
  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
        <li><a href="index.php">Overzicht</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
  
  <!--[if lt IE 7 ]>
    <script src="'js/dd_belatedpng.js" type="text/css" />
  <![endif]--> 
</div>
</body>
</html>