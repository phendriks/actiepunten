<?php

	include("connector.php");
	
	$query = mysql_query("SELECT * FROM `medewerkers`");
	
	if(!empty($_POST['submit']))
	{
		$err= '';
		if(empty($_POST['titel']))
		{
			$err .= "Je moet wel een titel invoeren.";
		}
				if(empty($_POST['tekst']))
		{
			$err .= "Je moet wel een tekst invoeren.";
		}
				if(empty($_POST['datum_up']))
		{
			$err .= "Je moet wel een datum invoeren.";
		}
				if(empty($_POST['eigenaar']))
		{
			$err .= "Je moet wel een eigenaar invoeren.";
		}
				if(empty($_POST['datum_eind']))
		{
			$err .= "Je moet wel een datum invoeren.";
		}
				if(empty($_POST['cat']))
		{
			$err .= "Je moet wel een categorie invoeren.";
		}
		if(empty($err))
		{
			$actp = " 	INSERT INTO
						`actiepunt`
						SET
						`titel`			=	'".$_POST['titel']."',
						`omschrijving`	=	'".$_POST['tekst']."',
						`update_datum`	=	'".$_POST['datum_up']."',
						`eind_datum`	=	'".$_POST['datum_eind']."',
						`eigenaar`		=	'".$_POST['eigenaar']."',
						`categorie`		=	'".$_POST['cat']."',
						`status`		=	'Nieuw'";
			mysql_query($actp);
			header("Location: " . $_SERVER['REQUEST_URI']);
		}
		else{ 
			echo $err;
			}
	}	
?>
<!doctype html>
<html lang="en" class="no-js">
<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Medewerkers - Actiepunten - KPN</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/base.css" type="text/css" />
<link rel="stylesheet" href="css/grid.css" type="text/css" />

<link rel="stylesheet" href="css/themes/light.css" type="text/css" />
<link rel="stylesheet" href="css/themes/green.css" type="text/css" />
<link rel="stylesheet" href="js/themes/metro/jtable_metro_base.min.css" type="text/css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" href="js/themes/metro/green/jtable.min.css" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="js/jquery.jtable.min.js"></script>
<script src="js/localization/jquery.jtable.nl-NL.js"></script>
<script type="text/javascript">
 $(function() {
    $( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
	
  });
</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
    <li><a href="index.php">Overzicht</a></li>
  <li class="current"><a href="actiepunten.php">Actiepunten</a><ul><li><a href="invoeren.php">Actiepunt invoeren</a></li></ul></li>
  <li><a href="medewerkers.php">Medewerkers</a></li>
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <h2 class="ribbon">Invoeren</h2>

  <div class="triangle-ribbon"></div>
  <div id="grid">
		<h2> Voer nieuw actiepunt in </h2>
		
		<form method="post" action="">
			<table id="invoerform">
			<tr><td>Titel:</td><td><input type="text" name="titel" /></td></tr>
			<tr><td>Omschrijving:</td><td><textarea name ="tekst" placeholder="Geef hier een duidelijke omschrijving op" cols="40" rows="10"></textarea></td></tr>
			<tr><td>Datum update:</td><td><input type="text" class="datepicker" name="datum_up"  /></td></tr>
			<tr><td>Eigenaar:</td><td><select name="eigenaar">
								<?php
									while($db= mysql_fetch_assoc($query))
									{
										echo "<option value=".$db['id'].">".$db['naam']."</option>";
									};
								?>
							</select>
							</td></tr>
			<tr><td>Verw. eind.</td><td><input type="text" class="datepicker" name="datum_eind" /></td></tr>
			<tr><td>Categorie:</td><td>		
								<select name="cat">
								<option value="Proces">Processen</option>
								<option value="Voice">Voice</option>
								<option value="Internet">Internet</option>
								<option value="TV">iTV</option>
								<option value="Operationeel">Operationeel</option>
								<option value="Hardware">Hardware</option>
								<option value="Tooling">Tooling</option>
								<option value="Levering">Levering</option>
							</select></td></tr>
			<tr><td></td><td><input type="submit" name="submit" value="Invoeren" /></td><td></td></tr>
			</table>
		</form>
  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
        <li><a href="index.php">Overzicht</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
  
  <!--[if lt IE 7 ]>
    <script src="'js/dd_belatedpng.js" type="text/css" />
  <![endif]--> 
</div>
</body>
</html>