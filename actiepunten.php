<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<?php
  if(!empty($_GET)){
	echo '<title>Actiepunt '.$_GET['id'].' - Actiepunten - KPN</title>';
  }
  else{
  echo '<title>Actiepunten - Actiepunten - KPN</title>';
  }
  ?>

<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png">

<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/base.css" type="text/css" />
<link rel="stylesheet" href="css/grid.css" type="text/css" />

<link rel="stylesheet" href="css/themes/light.css" type="text/css" />
<link rel="stylesheet" href="css/themes/green.css" type="text/css" />
<link rel="stylesheet" href="js/themes/metro/jtable_metro_base.min.css" type="text/css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" href="js/themes/metro/green/jtable.min.css" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="js/jquery.jtable.min.js"></script>
<script src="js/localization/jquery.jtable.nl-NL.js"></script>
<script type="text/javascript">
</script>
</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>



<!--<![endif]-->
<div id="wrapper" class="container_12"> 



<!-- start header -->
  <header> 
    <!-- logo -->
    <h1 id="logo"><a href="">KPN</a></h1>
    <!-- nav -->
       <nav>
<ul id="nav">
    <li><a href="index.php">Overzicht</a></li>
  <li class="current"><a href="actiepunten.php">Actiepunten</a><ul><li><a href="invoeren.php">Actiepunt invoeren</a></li><li><a href="archief.php">Archief</a></li></ul></li>
  <li><a href="medewerkers.php">Medewerkers</a></li>
</ul>
<br class="cl" />
    </nav>
  <br class="cl" />  
  </header>
  
  <div id="page">
  
  <?php
  include("connector.php");
    if(!empty($_GET)){

 
$query 		= mysql_query("SELECT
								`a`.`id`,
								`a`.`titel`,
								`a`.`omschrijving`,
								`a`.`update_datum`,
								`a`.`eind_datum`,
								`a`.`eigenaar`,
								`a`.`categorie`,
								`a`.`status`,
								`a`.`datum_invoer`,
								`m`.`naam`
						FROM 
								`actiepunt` AS `a`
						LEFT JOIN `medewerkers` AS `m` ON `a`.`eigenaar` = `m`.`id`	
						WHERE `a`.`id` = '".$_GET['id']."'	");
						
$actiepunt 	= mysql_fetch_assoc($query);	
	echo "<h2 class=ribbon>".$actiepunt['titel']."</h2>";				
 }

  else{
  echo "<h2 class=ribbon>Actiepunten</h2>";
  }
  ?>

  <div class="triangle-ribbon"></div>
  <div id="grid">
    <?php
	if(!empty($_GET)){
	
	
	$query2 	= mysql_query("	SELECT 
								`u`.`id`,
								`u`.`id_ap`,
								`u`.`tekst`,
								`u`.`update_datum`,
								`u`.`status`,
								`u`.`medewerker`,
								`m`.`naam`
								FROM 
								`updates` AS `u`
								LEFT JOIN `medewerkers` AS `m` ON `u`.`medewerker` = `m`.`id`
								WHERE 
								`id_ap` ='".$_GET['id']."' Order by u.update_datum DESC");
	
	
	$naam		= mysql_query(" SELECT * FROM `medewerkers`	");
	
	if(!empty($_POST['update']))
	{
		$err= '';
		if(empty($_POST['update']))
		{
			$err .= "Je moet wel een update invoeren.";
		}
		if(empty($err))
		{
			$update = "	INSERT INTO	`updates` SET
						id_ap 		=	'".$_GET['id']."',
						tekst 		=	'".$_POST['tekst']."',
						status		=	'".$_POST['status']."',
						medewerker 	=	'".$_POST['medewerker']."'";
			mysql_query($update);
			
			$status = " UPDATE 
						`actiepunt` 
						SET
						`Status` 	= '".$_POST['status']."'
						WHERE
						`id`		= '".$_GET['id']."'";
			mysql_query($status);
			header("Location: " . $_SERVER['REQUEST_URI']);
							
		}
		else{ 
			echo $err;
			}
	}
?>
<table id="overzicht" class="display" cellspacing="0">
<?php
			echo 
				"<tr><td><b>Eigenaar</b></td><td>".$actiepunt['naam']."</td></tr>
				<tr><td><b>Omschrijving</b></td><td>".$actiepunt['omschrijving']."</td></tr>
				<tr><td><b>Update datum</b></td><td>".$actiepunt['update_datum']."</td></tr>
				<tr><td><b>Eind datum</b></td><td>".$actiepunt['eind_datum']."</td></tr>
				<tr><td><b>Categorie</b></td><td>".$actiepunt['categorie']."</td></tr>
				<tr><td><b>Status</b></td><td>".$actiepunt['status']."</td></tr>
				<tr><td><b>Invoer datum</b></td><td>".$actiepunt['datum_invoer']."</td></tr></table><hr />";
				
			while($db= mysql_fetch_assoc($query2))
			{
				echo 
				"<table id=overzicht class=display cellspacing=0 width=100%>
				<tr><td><b>Update tekst</b></td><td>".	$db['tekst']."</td></tr>
				<tr><td><b>Naam</b></td><td>".$db['naam']."</td></tr>
				<tr><td><b>Status</b></td><td>".$db['status']."</td></tr>
				<tr><td><b>Update datum</b></td><td>".$db['update_datum']."</td></tr></table><hr />";
			};
		
		echo '
		<form method="post" action="">
			<textarea name ="tekst" placeholder="Geef hier een nieuwe update op" cols="40" rows="10"></textarea>
			<br />
			<select name="status">
				<option value="Voorbeelden"> Voorbeelden gezocht </option>
				<option value="Werk in uitvoering"> Werk in uitvoering </option>
				<option value="Vergeten"> Vergeten </option>
				<option value="Afgesloten"> Afgesloten </option>
			</select>
			<br />
			<select name="medewerker">';
				 while($da = mysql_fetch_assoc($naam))
				{
					echo "<option value=".$da['id'].">".$da['naam']."</option>";
				};
				
		echo '	</select>
			<hr />
			<input type="submit" name="update" value="Voer in" />
		</form>';
		}
		?>

  </div>
  </div>
    
  <footer>
    <ul class="footer-nav">
      <li><a href="index.php">Overzicht</a> |</li>
      <li><a href="http://www.kpn.nl">KPN</a></li>
    </ul>
    <p>Copyright &copy;2014, <a href="http://www.danakool.nl">Dana Kool</a></p>
    <br class="cl" />
  </footer>
  
  
  <!--[if lt IE 7 ]>
    <script src="'js/dd_belatedpng.js" type="text/css" />
  <![endif]--> 
</div>
</body>
</html>